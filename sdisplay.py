import displayio
import board
import busio
from adafruit_displayio_sh1106 import SH1106
from adafruit_display_text import label
import terminalio


class SimpleDisplay:

    def __init__(self) -> None:
        displayio.release_displays()
        self.i2c = busio.I2C(scl=board.SCL, sda=board.SDA)
        self.display_bus = displayio.I2CDisplay(self.i2c, device_address=0x3C)
        self.WIDTH = 128
        self.HEIGHT = 64
        self.display = SH1106(self.display_bus, width=self.WIDTH, height=self.HEIGHT)

    def set_root_group(self, group):
        self.display.root_group = group

    def get_width(self):
        return self.WIDTH
    
    def get_height(self):
        return self.HEIGHT
    
    def simple_center_text(self,text):
        group = displayio.Group()
        text_area = label.Label(terminalio.FONT, text=text, color=0xFFFFFF)
        text_area.x = self.get_width() // 2 - text_area.bounding_box[2] // 2
        text_area.y = self.get_height() // 2 - text_area.bounding_box[3] // 2
        group.append(text_area)
        self.display.root_group = group
