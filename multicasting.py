from socketpool import SocketPool
import wifi


class Multicast:

    def __init__(self,multicast_group = '224.3.0.1',multicast_port = 2000) -> None:
        self.multicast_group = multicast_group
        self.multicast_port = multicast_port
        self.pool = SocketPool(wifi.radio)
        self.sock = self.pool.socket(SocketPool.AF_INET, SocketPool.SOCK_DGRAM)
        self.sock.setsockopt(SocketPool.IPPROTO_IP,SocketPool.IP_MULTICAST_TTL , 2)

    def send(self,msg):
        self.sock.sendto(msg.encode('utf-8'), (self.multicast_group, self.multicast_port))

    def close(self):
        self.sock.close()
