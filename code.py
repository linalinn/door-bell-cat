import time, board, digitalio, wifi


from multicasting import Multicast
from secrets import secrets
from sdisplay import SimpleDisplay
import femtosip

# SIP Config
SIP_SERVER = "192.168.16.1"
SIP_PORT = 5060
SIP_PHONENUMBER_TO_CALL = "**610"
SIP_CALL_DURATION = 15.0

# Init IO
bell = digitalio.DigitalInOut(board.IO16)
bell.switch_to_input(digitalio.Pull.UP)

door = digitalio.DigitalInOut(board.IO17)
door.switch_to_input(digitalio.Pull.UP)

down = digitalio.DigitalInOut(board.IO18)
down.switch_to_input(digitalio.Pull.UP)


display = SimpleDisplay()
display.simple_center_text(f"connection to wifi\n {secrets['wifi']['ssid']}")

# Setup Networking
print("Connecting to Wifi")
wifi.radio.connect(secrets["wifi"]["ssid"], secrets["wifi"]["password"])
cast = Multicast()
sip = femtosip.SIP(secrets["sip"]["username"], secrets["sip"]["password"], SIP_SERVER, SIP_PORT, 'USB Nugget', local_ip=str(wifi.radio.ipv4_address))


# All setup
cast.send("boot: ok")
print("boot: ok")


def check_bell():
    if bell.value == False:
        print("ringring")
        cast.send("ringring")
        display.simple_center_text(f"Bell is ringing\n Calling {SIP_PHONENUMBER_TO_CALL}")
        sip.call(SIP_PHONENUMBER_TO_CALL, SIP_CALL_DURATION)


def check_door():
    if door.value == False:
        print("open")
        cast.send("open")
        display.simple_center_text("Door is open")
        I = 0
        while door.value == False:
            if I >= 10.0:
                print("open")
                cast.send("open")
                I = 0
            check_bell()
            display.simple_center_text("Door is open")
            time.sleep(0.5)
            I += 0.5
        print("closed")
        cast.send("closed")
        display.simple_center_text("Door is closed")

while True:
    if down.value == False:
        print("button down pressed!")
        display.simple_center_text(f"Calling {SIP_PHONENUMBER_TO_CALL}")
        sip.call(SIP_PHONENUMBER_TO_CALL, SIP_CALL_DURATION)
        time.sleep(0.5)
    if bell.value == False or door.value == False:
        check_bell()
        check_door()
        time.sleep(5)
    else:
        if door.value == True:
            display.simple_center_text("Door is closed")
        time.sleep(0.1)
