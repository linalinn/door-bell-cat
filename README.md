# Doorbell Cat implemented on USB Nugget

## SIP / VoIP
SIP works by hacking together to libs
### [femtosip](https://github.com/astoeckel/femtosip)
It was written for Python to be used for home automation together with a Fritz.Box router

It is used to send the SIP invitation (doing the call) it needed to be modified to work with `socketpool`. It uses uPySip for the md5 implementation.

Github: https://github.com/astoeckel/femtosip

### [uPySip](https://github.com/RetepRelleum/uPyVoip)
Is a VoIP / SIP lib for esp32 Micropython (this was my first choice. Supports: DTMF and playback) but it uses `socket.makefile` which is not implemented in Circutepython

Github: https://github.com/RetepRelleum/uPyVoip

## Features
 - SIP Call if the doorbell is ringing
 - Send multicast packages if the door is open or closed or the doorbell is ringing
 - Show state on the display